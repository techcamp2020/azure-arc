# Tutorial: Azure Arc enabled Kubernetes

This repository contains a self-contained tutorial for Azure Arc enabled Kubernetes. It also contains sample Kubernetes manifest files that can be deployed using GitOps to an Azure Arc enabled Kubernetes cluster. These are based on [Microsoft's official cluster configuration sample for Azure Arc enabled Kubernetes](https://github.com/Azure/arc-k8s-demo).

## Contents

| File/folder       | Description                                |
|-------------------|--------------------------------------------|
| `README.md`       | This README file. |
| `cluster-apps`    | Contains an example application that should be deployed to every cluster. |
| `namespaces`      | Contains three namespace resources to provision on an attached cluster. |
| `team-a`          | Contains a ConfigMap resource written into the `team-a` namespace and an exemplary deployment resource to install nginx into that same namespace. |

## Initial VM Setup

Use to provided Vagrantfile to spin up a CentOS 7 machine using Vagrant and Virtualbox.

```bash
vagrant up
vagrant ssh
```

### Install Required Packages

```bash
sudo yum install -y yum-utils wget
```

### Install Docker

```bash
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo systemctl start docker
```

Manage Docker as a non-root user

```bash
sudo usermod -aG docker $USER
```

Log out and log back in so that your group membership is re-evaluated.

### Install Go

```bash
wget https://golang.org/dl/go1.15.3.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.15.3.linux-amd64.tar.gz

# Add PATH=$PATH:/usr/local/go/bin:/home/vagrant/go/bin
vi ~/.bash_profile
source ~/.bash_profile
```

### Install kind and create Kubernetes Cluster

```bash
GO111MODULE="on" go get sigs.k8s.io/kind@v0.8.1

kind create cluster
```

### Install kubectl and kubeadm

```bash
sudo sh -c 'echo -e "[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg" > /etc/yum.repos.d/kubernetes.repo'

sudo yum install -y kubeadm kubectl
```

### Install Helm

```bash
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

### Install Azure CLI

```bash
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc

sudo sh -c 'echo -e "[azure-cli]
name=Azure CLI
baseurl=https://packages.microsoft.com/yumrepos/azure-cli
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/azure-cli.repo'

sudo yum install -y azure-cli

# Install extensions
az extension add --name connectedk8s
az extension add --name k8sconfiguration
```

## Connect Cluster to Azure Arc

```bash
az connectedk8s connect --name <cluster-name> --resource-group <resource-group-name>

# Example: az connectedk8s connect --name celestia-kubernetes --resource-group techcamp-arc
```

Source: [Microsoft Documentation](https://docs.microsoft.com/en-us/azure/azure-arc/kubernetes/connect-cluster#connect-a-cluster)

## Enable GitOps

This git repository contains an example configuration to be used. It can be forked an adapted.

```bash
az k8sconfiguration create --name cluster-config --cluster-name <cluster-name> --resource-group <resource-group-name> --operator-instance-name cluster-config --operator-namespace cluster-config --repository-url <repo-url> --scope cluster --cluster-type connectedClusters

# Example: az k8sconfiguration create --name cluster-config --cluster-name thalenia-cluster --resource-group techcamp-arc --operator-instance-name cluster-config --operator-namespace cluster-config --repository-url https://gitlab.com/techcamp2020/azure-arc --scope cluster --cluster-type connectedClusters
```

Validate the sourceControlConfiguration
 ```bash
az k8sconfiguration show --name cluster-config --cluster-name <cluster-name> --resource-group <resource-group-name> --cluster-type connectedClusters

# Example: az k8sconfiguration show --name cluster-config --cluster-name thalenia-cluster --resource-group techcamp-arc --cluster-type connectedClusters
 ```


Source: [Microsoft Documentation](https://docs.microsoft.com/en-us/azure/azure-arc/kubernetes/use-gitops-connected-cluster)


## Connecting the Kubernetes cluster to Azure Monitor

Microsoft provides a bash script for configuring the connection to Azure Monitor. You can download it using the command `curl -o enable-monitoring.sh -L https://aka.ms/enable-monitoring-bash-script`.

Configure a workspace for receiving the logs of the Kubernetes cluster using the [Azure Portal](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.OperationalInsights%2Fworkspaces) (if not done already for monitoring the host server).

Replace the placeholders in the following code snippet and execute the command:

```bash
enable-monitoring.sh --resource-id "/subscriptions/<subscriptionId>/resourceGroups/<resourceGroupName>/providers/Microsoft.Kubernetes/connectedClusters/<kubernetesClusterName>" \
    --workspace-id "/subscriptions/<subscriptionId>/resourceGroups/<resourceGroupName>/providers/microsoft.operationalinsights/workspaces/<monitorWorkspaceName>"
```

After about 15 minutes, the first metrics should appear in the Azure Portal. Go to [Azure Monitor](https://portal.azure.com/#blade/Microsoft_Azure_Monitoring/AzureMonitoringBrowseBlade/overview) --> [Containers](https://portal.azure.com/#blade/Microsoft_Azure_Monitoring/AzureMonitoringBrowseBlade/containerInsights) --> [Monitored Clusters](https://portal.azure.com/#blade/Microsoft_Azure_Monitoring/AzureMonitoringBrowseBlade/containerInsights) for having insights into the cluster.

Source: [Microsoft Documentation](https://docs.microsoft.com/en-us/azure/azure-monitor/insights/container-insights-enable-arc-enabled-clusters)

## Further Documentation

- [Azure Arc Kubernetes](https://docs.microsoft.com/en-us/azure/azure-arc/kubernetes/)
- [Kubernetes in Docker (Kind)](https://kind.sigs.k8s.io/)
- [Helm 3](https://helm.sh/docs/)